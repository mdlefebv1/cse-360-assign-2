/**
 * @author: Michelle Lefebvre
 * @classID: CSE360-85141
 * @assignment: 2
 * @description: This class tests the addingmachine class.
 *
 */

package cse360assign2;

import static org.junit.Assert.assertTrue;
import org.junit.jupiter.api.Test;

/**
 * Test cases for the addingmachine class
 * @author miche
 *
 */
class addingMachineTest {

	/**
	 * tests the toString to make sure right string is returned
	 */
	@Test
    void testtoString() {
        AddingMachine machine = new AddingMachine();
        machine.add (4);
        machine.subtract (2);
        machine.add(5);
        assertTrue("not 0 + 4 � 2 + 5", machine.toString().contentEquals("0 + 4 - 2 + 5"));
    }
	
	/**
	 * tests that the correct total is returned
	 */
	@Test
    void testgetTotal() {
        AddingMachine machine = new AddingMachine();
        machine.add (4);
        machine.subtract (2);
        machine.add(5);
        assertTrue("total not 7", machine.getTotal() == 7);
    }
	
	/**
	 * tests that the total is 0 after clear
	 */
	@Test
    void testclear() {
        AddingMachine machine = new AddingMachine();
        machine.add (4);
        machine.subtract (2);
        machine.add(5);
        machine.clear();
        assertTrue("total not 0", machine.getTotal() == 0);
    }
	
	/**
	 * tests that the value of total is 0 when no operations are performed
	 */
	@Test
    void testnothingAdded() {
        AddingMachine machine = new AddingMachine();
        assertTrue("total not 0", machine.getTotal() == 0);
    }

	/**
	 * tests a negative total value
	 */
	@Test
    void testNegative() {
        AddingMachine machine = new AddingMachine();
        machine.subtract (2);
        machine.subtract(5);
        assertTrue("total not -7", machine.getTotal() == -7);
    }
	
	/**
	 * tests a large amount of operations returns correct total
	 */
	@Test
    void testLarge() {
        AddingMachine machine = new AddingMachine();
        machine.add(20);
        machine.subtract(5);
        machine.add(2);
        machine.subtract(1);
        machine.add(20);
        machine.add(5);
        machine.subtract(10);
        machine.subtract(5);
        assertTrue("total not 26", machine.getTotal() == 26);
        
    }
	
	/**
	 * tests that a large amount of operations returns correct toString
	 */
	@Test
    void testLargetoString() {
        AddingMachine machine = new AddingMachine();
        machine.add(20);
        machine.subtract(5);
        machine.add(2);
        machine.subtract(1);
        machine.add(20);
        machine.add(5);
        machine.subtract(10);
        machine.subtract(5);
        assertTrue("total not 0 + 20 - 5 + 2 - 1 + 20 + 5 - 10 - 5", machine.toString().contentEquals("0 + 20 - 5 + 2 - 1 + 20 + 5 - 10 - 5"));
        
    }
}
