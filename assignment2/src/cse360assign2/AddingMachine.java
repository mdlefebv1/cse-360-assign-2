/**
 * @author: Michelle Lefebvre
 * @classID: CSE360-85141
 * @assignment: 2
 * @description: this program adds and subtracts from the total. and is also capable of returnin the stored total,
 * clearing total, and can return a string of all operations done. In short, this si a basic add and subtract calculator
 *
 */
package cse360assign2;

/**
 * This class adds, subtracts from a given total and displays total and operations to user.
 */
public class AddingMachine {
	

    /**
     * The numeric total after operations are performed.
     */
    private int total;
    /**
     * The string of operations performed.
     */
    private String operationString = "0";

    /**
     * constructor for class. Initializes total to 0;
     */
    public AddingMachine () {
        total = 0;  // not needed - included for clarity
    }

    /**
     * returns the total
     * @return total
     */
    public int getTotal () {
        return total;
    }

    /**
     * adds the given value to the total value
     * @param value
     */
    public void add (int value) {
    		total = total + value;
    		operationString = operationString + " + " + value;
    		
    }

    /**
     * subtracts the given value from the total value
     * @param value
     */
    public void subtract (int value) {
    	total = total - value;
    	operationString = operationString + " - " + value;
    }

    /**
     * returns a string of all operations performed
     * @return operationString : the string contains all operations and values added/subtracted from total
     */
    public String toString () {
        return operationString;
    }

    /**
     * sets the total back to 0
     */
    public void clear() {
    	total = 0;
    	operationString = "0";

    }
}

